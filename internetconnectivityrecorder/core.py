import logging
import requests
from requests.adapters import HTTPAdapter
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Sequence, Integer, DateTime, String
from datetime import datetime, timedelta
import locale
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib.ticker as ticker
import matplotlib.dates as mdates

_logger = logging.getLogger(__name__)
Base = declarative_base()


class Core:
    def __init__(self, check, chart, database_file):
        self._database_file = database_file
        self._check = check
        self._chart = chart

        database_string = 'sqlite:///' + str(database_file)
        engine = create_engine(database_string)
        Base.metadata.create_all(engine)
        self._Session = sessionmaker(bind=engine)

    def is_internet_ok(self):
        session = requests.Session()
        session.mount('https://', HTTPAdapter(max_retries=0))
        try:
            response = session.get(self._check['website'], timeout=self._check['timeout'])
        except Exception as exception:
            _logger.warning(exception)
            _logger.debug("Exception class: %s", exception.__class__)
            _logger.debug("Exception name: %s", exception.__class__.__name__)
            return False
        else:
            _logger.debug(response)
            return True

    def record(self, state):
        _logger.info("Recording check: %s", state)
        check = Check(timestamp=datetime.now(), state=state)
        session = self._Session()
        session.add(check)
        session.commit()
        session.close()

    def show_chart(self):
        self._draw_chart()
        plt.show()

    def save_chart(self, file):
        self._draw_chart()
        plt.savefig(file)

    def _draw_chart(self):
        connectivity = self.get_connectivity()
        locale.setlocale(locale.LC_ALL, self._chart['time_locale'])  # name might vary with platform
        plt.title(self._chart['title'])
        plt.plot(connectivity['time'], connectivity['quality'], linewidth=self._chart['line_width'])
        ax = plt.gca()
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax.set_ylim([0, 100])
        ax.xaxis.set_major_locator(ticker.MaxNLocator(self._chart['max_ticks']))
        ax.xaxis.set_major_formatter(mdates.DateFormatter(self._chart['time_tick_format']))

    def get_connectivity(self):
        checks = self.get_checks()

        check_total = dict()
        check_ok = dict()
        first_timestamp = None
        last_timestamp = None
        for check in checks:
            timestamp = check.timestamp

            if not first_timestamp or timestamp < first_timestamp:
                first_timestamp = timestamp
            if not last_timestamp or last_timestamp < timestamp:
                last_timestamp = timestamp

            group = timestamp.strftime(self._chart['group_format'])
            if group not in check_total:
                check_total[group] = 0
            check_total[group] += 1

            if group not in check_ok:
                check_ok[group] = 0
            if check.state == 'ok':
                check_ok[group] += 1

        _logger.debug("Total ok: %s", check_ok)
        _logger.debug("Total checks: %s", check_total)

        first_truncated = datetime(
            first_timestamp.year, first_timestamp.month, first_timestamp.day, first_timestamp.hour
        )
        last_truncated = datetime(
            last_timestamp.year, last_timestamp.month, last_timestamp.day, last_timestamp.hour
        )

        _logger.debug("First hour: %s", first_truncated)
        _logger.debug("Last hour: %s", last_truncated)

        time = list()
        quality = list()
        ok_list = list()
        total_list = list()
        dt = first_truncated
        while dt <= last_truncated:
            group = dt.strftime(self._chart['group_format'])
            ok = check_ok.get(group)
            total = check_total.get(group)

            if total is None:
                quality.append(None)
            else:
                quality.append(100 * ok / total)

            time.append(dt)

            ok_list.append(ok)
            total_list.append(total)

            dt = dt + timedelta(hours=1)

        _logger.debug("Quality: %s", quality)
        _logger.debug("Hours: %s", time)

        return dict(quality=quality, time=time, ok=ok_list, total=total_list)

    def get_checks(self):
        session = self._Session()
        checks = session.query(Check).all()
        session.close()
        return checks


class Check(Base):
    __tablename__ = 'checks'
    id = Column(Integer, Sequence('check_id_seq'), primary_key=True)
    timestamp = Column(DateTime)
    state = Column(String)

    def __repr__(self):
        return "<Check(id='{}', timestamp='{}', state={})>".format(self.id, self.timestamp, self.state)
