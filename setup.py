from setuptools import setup
import internetconnectivityrecorder

setup(
    name='internet-connectivity-recorder',
    version=internetconnectivityrecorder.__version__,
    packages=['internetconnectivityrecorder'],
    url='',
    license='',
    author='Luc PHAN',
    author_email='',
    description='',
    entry_points={'console_scripts': ['internet-connectivity-recorder = internetconnectivityrecorder.command:cli']},
    install_requires=['click', 'pyyaml', 'requests', 'sqlalchemy', 'matplotlib', 'tabulate'],
    include_package_data=True
)
