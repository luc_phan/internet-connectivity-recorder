import logging.config
import click
from pathlib import Path
import shutil
import yaml
from tabulate import tabulate
from .core import Core
from . import __version__


_LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            # 'format': "[%(asctime)s][%(levelname)s] %(message)s"
            'format': "[%(levelname)s] %(message)s"
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        }
    },
    'loggers': {
        'internetconnectivityrecorder': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    }
}
logging.config.dictConfig(_LOGGING)
_logger = logging.getLogger(__name__)


def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    command = Command()
    command.print_version()
    ctx.exit()


@click.group()
@click.option('--version', '-v', '-V', is_flag=True, callback=print_version, expose_value=False, is_eager=True)
def cli():
    pass


@click.command()
def check():
    command = Command()
    command.check()


@click.command()
def show():
    command = Command()
    command.show()


@click.command()
def stats():
    command = Command()
    command.stats()


@click.group()
def chart():
    pass


@click.command()
def chart_show():
    command = Command()
    command.chart_show()


@click.command()
def chart_save():
    command = Command()
    command.chart_save()


cli.add_command(check)
cli.add_command(show)
cli.add_command(stats)
cli.add_command(chart)
chart.add_command(chart_show, name='show')
chart.add_command(chart_save, name='save')


class Command:
    _CONFIG_DIRECTORY_NAME = '.internet-connectivity-recorder'
    _CONFIG_FILE_NAME = 'config.yml'
    _CONFIG_DISTRIBUTION_FILE = 'data/config.dist.yml'
    _DATABASE_FILE_NAME = 'data.sqlite3'
    _CHART_FILE_NAME = 'internet-connectivity-chart.png'

    def __init__(self):
        config_directory = Path.home() / self._CONFIG_DIRECTORY_NAME
        config = Config(
            directory=config_directory,
            file=config_directory / self._CONFIG_FILE_NAME,
            distribution_file=Path(__file__).parent / self._CONFIG_DISTRIBUTION_FILE
        )
        config.create()
        config_dict = config.load()
        logging.config.dictConfig(config_dict['logging'])
        _logger.debug("Configuration: %s", config_dict)

        self._core = Core(
            check=config_dict['check'],
            chart=config_dict['chart'],
            database_file=config_directory / self._DATABASE_FILE_NAME
        )

    @staticmethod
    def print_version():
        click.echo("internet-connectivity-recorder v" + __version__)

    def check(self):
        _logger.info("Checking connectivity...")
        if self._core.is_internet_ok():
            click.echo("Internet: Ok")
            self._core.record('ok')
        else:
            click.echo("Internet: KO!")
            self._core.record('nok')

    def show(self):
        _logger.info("Showing checks...")
        checks = self._core.get_checks()

        rows = [[check_.id, check_.timestamp, check_.state] for check_ in checks]
        table = tabulate(rows, headers=['id', 'timestamp', 'state'], tablefmt='psql')
        click.echo(table)

    def stats(self):
        _logger.info("Showing stats...")
        connectivity = self._core.get_connectivity()

        rows = [
            [time, quality, ok, total] for time, quality, ok, total in zip(
                connectivity['time'],
                connectivity['quality'],
                connectivity['ok'],
                connectivity['total']
            )
        ]
        table = tabulate(rows, headers=['Time', 'Connectivity', 'OK', 'Total'], tablefmt='psql')
        click.echo(table)

    def chart_show(self):
        _logger.info("Showing chart...")
        self._core.show_chart()

    def chart_save(self):
        _logger.info("Saving chart: %s", self._CHART_FILE_NAME)
        chart_file = Path(self._CHART_FILE_NAME)
        if chart_file.exists():
            raise RuntimeError("File exists: " + str(chart_file))
        self._core.save_chart(chart_file)


class Config:
    def __init__(self, directory, file, distribution_file):
        self._directory = directory
        self._file = file
        self._distribution_file = distribution_file

    def create(self):
        if not self._directory.is_dir():
            _logger.info("Creating config directory: %s", self._directory)
            self._directory.mkdir()
        if not self._file.is_file():
            _logger.info("Creating config file: %s", self._file)
            shutil.copyfile(self._distribution_file, self._file)

    def load(self):
        with self._file.open(encoding='utf-8') as f:
            return yaml.safe_load(f)
