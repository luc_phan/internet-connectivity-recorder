internet-connectivity-recorder
==============================

Description
-----------

**internet-connectivity-recorder** is a shell command to check if
you're connected to the internet and record it into an *SQLite3*
database:

```
$ internet-connectivity-recorder check
[INFO] Checking connectivity...
Internet: Ok
[INFO] Recording check: ok
$ ls ~/.internet-connectivity-recorder/data.sqlite3
/home/luc2/.internet-connectivity-recorder/data.sqlite3
```

It's designed to run in a *cron*.

Then it can output a table to summarize the quality of your connection:

```
$ internet-connectivity-recorder show
[INFO] Showing checks...
+------+----------------------------+---------+
|   id | timestamp                  | state   |
|------+----------------------------+---------|
|    1 | 2020-03-23 14:20:22.288927 | ok      |
|    2 | 2020-03-23 14:21:26.928891 | ok      |
|    3 | 2020-03-23 14:23:04.408932 | nok     |
|    4 | 2020-03-23 14:24:33.336510 | ok      |
|    5 | 2020-03-23 14:25:35.120233 | ok      |
|    6 | 2020-03-23 14:26:36.834257 | ok      |
|    7 | 2020-03-23 14:27:38.571257 | ok      |
|    8 | 2020-03-23 14:28:40.189233 | ok      |
...
```

It can also output stats by hour:

```
$ internet-connectivity-recorder stats
[INFO] Showing stats...
+---------------------+----------------+------+---------+
| Time                |   Connectivity |   OK |   Total |
|---------------------+----------------+------+---------|
| 2020-03-23 14:00:00 |        75      |   21 |      28 |
| 2020-03-23 15:00:00 |        53.3333 |   24 |      45 |
| 2020-03-23 16:00:00 |        94.2308 |   49 |      52 |
| 2020-03-23 17:00:00 |        92.7273 |   51 |      55 |
| 2020-03-23 18:00:00 |        76.4706 |   39 |      51 |
| 2020-03-23 19:00:00 |        70.2128 |   33 |      47 |
| 2020-03-23 20:00:00 |        54.5455 |   24 |      44 |
...
```

It can also draw a chart:

```
$ internet-connectivity-recorder chart show
[INFO] Showing chart...
```

![Connectivity chart](images/internet-connectivity-chart.png)

It's also possible to save the chart to a file :

```
$ internet-connectivity-recorder chart save
[INFO] Saving chart: internet-connectivity-chart.png
```

Installation
------------

1. Install Python 3.
2. Install PIP 3.
3. Download `internet-connectivity-recorder-0.10.0.tar.gz`.
4. Type `pip3 install --user internet-connectivity-recorder-0.8.0.tar.gz`.
5. Type `internet-connectivity-recorder` to check the shell command works.

Configuration
-------------

```
$ vim ~/.internet-connectivity-recorder/config.yml
```

```
---
check:
    website: https://www.google.com/
    timeout: 10
chart:
    title: Connectivity
    group_format: '%Y-%m-%d %H'
    time_tick_format: '%Hh'
    time_locale: en_US
    line_width: 10
logging:
    version: 1
    disable_existing_loggers: True
    formatters:
        standard:
#            format: "[%(asctime)s][%(levelname)s] %(message)s"
            format: "[%(levelname)s] %(message)s"
    handlers:
        console:
            level: DEBUG
            class: logging.StreamHandler
            formatter: standard
    loggers:
        internetconnectivityrecorder:
            handlers: ['console']
            level: INFO
...
```

Development
-----------

### Text table packages

#### Packages

- tabulate
- ~~PrettyTable~~
- ~~texttable~~

#### References

- https://stackoverflow.com/questions/9535954/printing-lists-as-tabular-data
- https://www.educative.io/edpresso/print-a-table-in-python

TODO
----

- ~~Add version to shell command~~
- ~~Disable requests retry~~
- ~~Add feature to display checks table~~
- Add ok and total to stats
